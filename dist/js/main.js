var btn            = document.querySelector('button'),
    task           = document.querySelector('#todo'),
    ulTodo         = document.querySelector('#ul-todo'),
    ulFilter       = document.querySelector('.collection'),
    allInputFilter = document.querySelectorAll('.filter'),
    btnDelete      = document.querySelector('#btn-delete'),
    validSupp      = document.querySelector('#valid'),
    cancelSupp     = document.querySelector('#cancel');

var Task = function(check, date, updated) {
    this.check   = check
    this.date    = date
    this.updated = updated
}

var data = {}


window.onload = function(e) {
    var nbTaskopened = 0
    if (localStorage.length > 0) {
        var tabTasks = Object
            .keys(localStorage)
            .sort(function(first, next) {
                return new Date(JSON.parse(localStorage[first]).date) - new Date(JSON.parse(localStorage[next]).date);
            })
            tabTasks.forEach(function(task) {
                addTodo(task.replace(/_/g, " "), JSON.parse(localStorage[task]), function(data) {
                    if(!data.check) nbTaskopened++
                })
        })
        checkDelete()
        switch(nbTaskopened) {
            case 0:
                Materialize.toast('Welcome, you have completed all tasks !', 4000)
                break;
            case 1:
                Materialize.toast('Welcome, you have ' + nbTaskopened + ' task in progress !', 4000)
                break;

            default:
                Materialize.toast('Welcome, you have ' + nbTaskopened + ' tasks in progress !', 4000)
                break;
        }
    }else Materialize.toast('Welcome !', 4000)
}

btn.addEventListener('click', function(e) {
    e.preventDefault()

    if (task.value !== '') {
        if(!localStorage.getItem(task.value.toLowerCase().replace(/\s/g,'_'))) {
            var inputTask = task.value
            task.value = ""
            
            newTask = new Task(false, new Date().getTime(), '')
        
            addTodo(inputTask, newTask, function(res) {
                Materialize.toast('Your task was added successfully', 4000, 'green')
            })
            save(inputTask, newTask)
        }else {
            Materialize.toast('Your task is already exist', 4000, 'red')
            task.value = ""
        }
    }
})

allInputFilter.forEach(function(element) {
    if(element.id === 'all') element.checked = true

    element.addEventListener('change', function(e) {
        var allInputTodo = document.querySelectorAll('.task')
        switch (element.id) {
            case 'all':
                allInputTodo.forEach(function(el) {
                    el.parentElement.classList.remove('hidden')
                })
                break;
            case 'opened':
                allInputTodo.forEach(function(el) {
                    if(el.hasAttribute('checked')) el.parentElement.classList.add('hidden')
                    if(!el.hasAttribute('checked')) el.parentElement.classList.remove('hidden')
                })
                break;
        
            case 'closed':
                allInputTodo.forEach(function(el) {
                    if(!el.hasAttribute('checked')) el.parentElement.classList.add('hidden')
                    if(el.hasAttribute('checked')) el.parentElement.classList.remove('hidden')
                })
                break;
        }
    })
})

btnDelete.addEventListener('click', function(e) {
    mbox.confirm('Delete all tasks checked ?', function(res) {
        var allInputTodo = document.querySelectorAll('.task')
        if(res) {
            allInputTodo.forEach(function(el) {
                if(el.checked) {
                    el.parentElement.remove()
                    var key = el.nextElementSibling.textContent.toLowerCase().replace(/\s/g,'_')
                    localStorage.removeItem(key)
                }
            })
            checkDelete()
        }
    })
})

////>>> FUNCTIONS <<<////
/**
 * Add new task, return Task object
 * @param {String} newTask 
 * @param {{}} data 
 * @param {Function} cb 
 * @returns {{}}
 */
function addTodo(newTask, data, cb) {
    var liTodo = document.createElement('li')
    liTodo.setAttribute('class', 'collection-item')

    var inputTodo = document.createElement('input')
    inputTodo.addEventListener('change', function(e) {
        checkTask(newTask, data, inputTodo, spanTodo, liTodo, e)
        checkDelete()
    })
    inputTodo.setAttribute('type', 'checkbox')
    inputTodo.setAttribute('class', 'task')
    inputTodo.setAttribute('id', newTask.toLowerCase().replace(/\s/g,'_'))
    
    var labelTodo = document.createElement('label')
    labelTodo.setAttribute('for', newTask.toLowerCase().replace(/\s/g,'_'))
    labelTodo.textContent = capitalize(newTask)
    
    var spanTodo = document.createElement('span')
    spanTodo.classList.add('class', 'date')
    spanTodo.textContent = new Date(data.date).toLocaleString('fr-FR')
    
    checkTask(newTask, data, inputTodo, spanTodo, liTodo, false)
    
    liTodo.appendChild(spanTodo)
    liTodo.appendChild(inputTodo)
    liTodo.appendChild(labelTodo)
    ulTodo.insertBefore(liTodo, ulTodo.childNodes[2])

    // Callback
    cb(data)
}

function checkTask(newTask, data, inputTodo, spanTodo, liTodo, event) {
    switch(event) {
        case false:
            if(data.check) {
                inputTodo.setAttribute('checked', '')
                elapsedTime(data.date, data.updated, function(diffMessage) {
                    spanTodo.textContent = diffMessage
                })
                liTodo.classList.add('li-checked')
            }else {
                inputTodo.removeAttribute('checked')
                spanTodo.textContent = 'Le ' + new Date(data.date).toLocaleString('fr-FR')
                liTodo.classList.remove('li-checked')
            }
            break;
        default:
            if(!event.target.checked) {
                data.check   = false
                data.updated = ''
                save(newTask, data)
    
                inputTodo.removeAttribute('checked')
                spanTodo.textContent = 'Le ' + new Date(data.date).toLocaleString('fr-FR')
                liTodo.classList.remove('li-checked')
            }else {
                data.check   = true
                data.updated = new Date().getTime()
                save(newTask, data)
                
                inputTodo.setAttribute('checked', '')
                elapsedTime(data.date, data.updated, function(diffMessage) {
                    spanTodo.textContent = diffMessage
                })
                liTodo.classList.add('li-checked')
            }
            break;
    }
}

/**
 * 
 * @param {String} task
 * @param {{}} data
 */
function save(task, data) {
    task = task.toLowerCase().replace(/\s/g,'_')
    data = JSON.stringify(data)
    localStorage.setItem(task, data)
}

/**
 * 
 * @param {String} str 
 * @returns {String}
 */
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

/**
 * 
 * @param {Date} start 
 * @param {Date} end 
 * @param {Function} cb 
 * @returns {String}
 */
function elapsedTime(start, end, cb) {
    var elapsed = (end - start) / 1000

    if(elapsed >= 0) {
        var diff = {}

        diff.days    = Math.floor(elapsed / 86400)
        diff.hours   = Math.floor(elapsed / 3600 % 24)
        diff.minutes = Math.floor(elapsed / 60 % 60)
        diff.seconds = Math.floor(elapsed % 60)

        var message = 'Time elapsed '+ diff.days +'d '+ diff.hours +'h '+ diff.minutes +'m '+ diff.seconds +'s !'
        message = message.replace(/(?:0. )+/, '')

        cb(message)
    }
}

/**
 * Enable/Disabled Delete btn
 */
function checkDelete() {
    var allInputTodo = document.querySelectorAll('.task')
    var ifCheck = 0
    allInputTodo.forEach(function(el) {
        if(el.checked) ifCheck++
        ifCheck ? btnDelete.classList.remove('disabled') : btnDelete.classList.add('disabled')
    });
}
